/// <reference types="cypress" />

import * as dados from "../../fixtures/example.json";
import { credencias } from "../../../cypress.env";

function goUrl() {
    cy.visit(`${dados.urlBase}/login`);
}

export { goUrl }
class login {

    goUrl() {
        return cy.visit(`${dados.urlBase}/login`);
    }
  
    preencherLogin() {
        cy.get('input[id="login-field"]').type(credencias.login);
        cy.get('input[id="login-password"]').type(credencias.Senha);
    }

    clickEntrar() {
        cy.get('div button[data-testid="login-button"]').click();
    }


}

export default new login()
