// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import * as dados from "../fixtures/example.json";
import { credencias } from "../../cypress.env";

Cypress.Commands.add('login', (email, password) => {
    cy.visit(`${dados.urlBase}/app/acessar`);
    cy.get('input[id="login-field"]').type(credencias.login);
    cy.get('input[id="login-password"]').type(credencias.Senha);
    cy.get('div button[data-testid="login-button"]').click();
})